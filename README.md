# SyVANIR #



### What is SyVANIR? ###

**SyVANIR** (**V**erifiable **A**ccess to **N**FTs **I**nstilled with **R**eality) is an artistic collection of digital 3D model **NFT**s (**N**on-**f**ungible **t**okens) based on scientific phenomena.